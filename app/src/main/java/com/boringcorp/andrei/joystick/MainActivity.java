package com.boringcorp.andrei.joystick;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements JoystickView.JoystickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //JoystickView joystickView = new JoystickView(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onJoystickMoved(float xPercent, float yPercent, int source) {

        int xVal = (int) (xPercent * 1000);
        int yVal = (int) (yPercent * 1000);


        byte xMapped = mapValue(xVal, -1000, 1000, 0, 255);
        byte yMapped = mapValue(yVal, -1000, 1000, 0, 255);

        final byte a1 = xMapped;
        final byte a2 = yMapped;

        Log.d("Main method", "X val: " + xMapped + " Y val: " + yMapped);

        if(FindDevice.bluetoothSocket != null) {
            try {
                FindDevice.bluetoothSocket.getOutputStream().write(a1);
                FindDevice.bluetoothSocket.getOutputStream().write(a2);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.connect, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_connect) {
            startActivity(new Intent(MainActivity.this, FindDevice.class));
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    //
//    OldRange = (OldMax - OldMin)
//            if (OldRange == 0)
//    NewValue = NewMin
//    else
//    {
//        NewRange = (NewMax - NewMin)
//        NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin
//    }
    public byte mapValue(int oldVal, int oldMin, int oldMax, int newMin, int newMax) {
        int oldRange = oldMax - oldMin;

        if (oldRange == 0) {
            return (byte)newMin;
        } else {
            int newRange = newMax - newMin;
            int newValue = (((oldVal - oldMin) * newRange) / oldRange) + newMin;
            return (byte)newValue;
        }
    }

    private void alertMessage(String message){
        Toast.makeText(MainActivity.this,message,Toast.LENGTH_LONG).show();
    }


//    @Override
//    protected void onStop() {
//        try {
//            if(FindDevice.bluetoothSocket != null) {
//                FindDevice.bluetoothSocket.close();
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        super.onStop();
//    }
}